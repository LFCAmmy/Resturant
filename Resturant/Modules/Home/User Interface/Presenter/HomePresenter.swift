//
//  HomePresenter.swift
//  Resturant
//
//  Created by Armaan Shrestha on 10/03/2022.
//
//

import Foundation

class HomePresenter {
    
	// MARK: Properties
    
    weak var view: HomeViewInterface?
    var interactor: HomeInteractorInput?
    var wireframe: HomeWireframeInput?

    // MARK: Converting entities
    private func convertInput(models: [FoodStructure]) -> [FoodViewModel] {
        models.map({ model -> FoodViewModel in
            let viewModel = FoodViewModel(id: model.id, category: model.category, name: model.name, price: model.price, image: model.image)
            return viewModel
        })
    }
}

 // MARK: Home module interface

extension HomePresenter: HomeModuleInterface {
    func getFood() {
        interactor?.getFood()
    }
    
    func calculateBill(cartList: [Cart]) {
        interactor?.calculateBill(cartList: cartList)
    }
    
    func addToCart(food: FoodViewModel, cartList: [Cart]) {
        interactor?.addToCart(food: food, cartList: cartList)
    }
    
    func removeFromCart(food: FoodViewModel, cartList: [Cart]) {
        interactor?.removeFromCart(food: food, cartList: cartList)
    }
    
    func qtyUpdated(qty: Int, food: FoodViewModel, cartList: [Cart]) {
        interactor?.qtyUpdated(qty: qty, food: food, cartList: cartList)
    }
}

// MARK: Home interactor output interface

extension HomePresenter: HomeInteractorOutput {
    func getFoodItems(structure: [FoodStructure]) {
        let viewModel = convertInput(models: structure)
        view?.getFoodItems(viewModel: viewModel)
    }
    
    func obtained(subTotal: Int, taxAmount: Int, totalAmount: Int) {
        view?.obtained(subTotal: subTotal, taxAmount: taxAmount, totalAmount: totalAmount)
    }
    
    func obtained(structure: [Cart]) {
        view?.obtained(viewModel: structure)
    }
}
