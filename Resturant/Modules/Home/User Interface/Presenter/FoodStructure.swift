//
//  FoodStructure.swift
//  Resturant
//
//  Created by Armaan Shrestha on 11/03/2022.
//

import Foundation

struct FoodStructure {
    var id: Int
    var category: String
    var name: String
    var price: Int
    var image: String
}
