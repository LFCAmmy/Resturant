//
//  CartCollectionViewCell.swift
//  Resturant
//
//  Created by Armaan Shrestha on 10/03/2022.
//

import UIKit

class CartCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var minusBtn: UIButton!
    @IBOutlet weak var qtyTextField: UITextField!
    @IBOutlet weak var plusBtn: UIButton!
    
    var handleMinusBtnAction: (() -> Void)?
    var handleAddBtnAction: (() -> Void)?
    var handleQtyEdited: (() -> Void)?
    
    @IBAction func minusBtnClicked(_ sender: Any) {
        handleMinusBtnAction?()
    }
    
    @IBAction func plusBtnClicked(_ sender: Any) {
        handleAddBtnAction?()
    }
    
    @IBAction func textChanged(_ sender: Any) {
        handleQtyEdited?()
    }
    
    func renderData(food: FoodViewModel) {
        categoryLabel.text = food.category
        nameLabel.text = food.name
        priceLabel.text = String(food.price).currency()
        image.image = UIImage(named: food.image)
    }
    
    func updateQuantity(isQtyAdded : Bool = true, food: FoodViewModel) {
        var qty = Int(qtyTextField.text!)!
        
        if isQtyAdded {
            qty += 1
        } else {
            if qty > 0 {
                qty -= 1
            }
        }
        
        qtyTextField.text = String(qty)
    }
    
}
