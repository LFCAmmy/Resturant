//
//  HomeViewController.swift
//  Resturant
//
//  Created by Armaan Shrestha on 10/03/2022.
//
//

import UIKit

class HomeViewController: UIViewController {
    
    var tableData = [FoodViewModel]() {
        didSet {
            collectionView.reloadData()
        }
    }
    
    var cartList = [Cart]()
    
    // MARK: Properties
    
    var presenter: HomeModuleInterface?
    
    // MARK: IBOutlets
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var subTotalLabel: UILabel!
    @IBOutlet weak var taxLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var payNowBtn: UIButton!
    
    
    // MARK: VC's Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
        presenter?.getFood()
        presenter?.calculateBill(cartList: cartList)
    }
    
    // MARK: IBActions
    @IBAction func payNowBtnClicked(_ sender: Any) {
    }
    
    // MARK: Other Functions
    
    private func setup() {
        // all setup should be done here
        setupDesign()
    }
    
    func setupDesign() {
        payNowBtn.rounded()
    }
    
}

// MARK: HomeViewInterface
extension HomeViewController: HomeViewInterface {
    func obtained(viewModel: [Cart]) {
        cartList = viewModel
    }
    
    func getFoodItems(viewModel: [FoodViewModel]) {
        tableData = viewModel
    }
    
    func obtained(subTotal: Int, taxAmount: Int, totalAmount: Int) {
        subTotalLabel.text = String(subTotal).currency()
        taxLabel.text = String(taxAmount).currency()
        totalLabel.text = String(totalAmount).currency()
    }
    
}

extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tableData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CartCollectionViewCell", for: indexPath) as! CartCollectionViewCell
        let food = tableData[indexPath.item]
        cell.renderData(food: food)
        cell.handleMinusBtnAction = {
            cell.updateQuantity(isQtyAdded: false, food: food)
            self.presenter?.removeFromCart(food: food, cartList: self.cartList)
            self.presenter?.calculateBill(cartList: self.cartList)
        }
        cell.handleAddBtnAction = {
            cell.updateQuantity(food: food)
            self.presenter?.addToCart(food: food, cartList: self.cartList)
            self.presenter?.calculateBill(cartList: self.cartList)
        }
        cell.handleQtyEdited = {
            guard let qty = cell.qtyTextField.text, !qty.isEmpty else {
                self.presenter?.qtyUpdated(qty: 0, food: food, cartList: self.cartList)
                self.presenter?.calculateBill(cartList: self.cartList)
                return
            }
            self.presenter?.qtyUpdated(qty: Int(qty)!, food: food, cartList: self.cartList)
            self.presenter?.calculateBill(cartList: self.cartList)
        }
        return cell
    }
    
}
