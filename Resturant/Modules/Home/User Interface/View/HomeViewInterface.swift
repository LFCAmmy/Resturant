//
//  HomeViewInterface.swift
//  Resturant
//
//  Created by Armaan Shrestha on 10/03/2022.
//
//

protocol HomeViewInterface: class {
    func getFoodItems(viewModel: [FoodViewModel])
    func obtained(subTotal: Int, taxAmount: Int, totalAmount: Int)
    func obtained(viewModel: [Cart])
}
