//
//  HomeWireframe.swift
//  Resturant
//
//  Created by Armaan Shrestha on 10/03/2022.
//
//

import UIKit

class HomeWireframe {
     weak var view: UIViewController!
}

extension HomeWireframe: HomeWireframeInput {
    
    var storyboardName: String {return "Home"}
    
    func getMainView() -> UIViewController {
        let service = HomeService()
        let interactor = HomeInteractor(service: service)
        let presenter = HomePresenter()
        let viewController = viewControllerFromStoryboard(of: HomeViewController.self)
        
        viewController.presenter = presenter
        interactor.output = presenter
        presenter.interactor = interactor
        presenter.wireframe = self
        presenter.view = viewController
        
        self.view = viewController
        return viewController
    }
}
