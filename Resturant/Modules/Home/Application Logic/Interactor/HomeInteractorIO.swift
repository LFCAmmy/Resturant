//
//  HomeInteractorIO.swift
//  Resturant
//
//  Created by Armaan Shrestha on 10/03/2022.
//
//

protocol HomeInteractorInput: class {
    func getFood()
    func calculateBill(cartList: [Cart])
    func addToCart(food: FoodViewModel, cartList: [Cart])
    func removeFromCart(food: FoodViewModel, cartList: [Cart])
    func qtyUpdated(qty: Int, food: FoodViewModel, cartList: [Cart])
}

protocol HomeInteractorOutput: class {
    func getFoodItems(structure: [FoodStructure])
    func obtained(subTotal: Int, taxAmount: Int, totalAmount: Int)
    func obtained(structure: [Cart])
}
