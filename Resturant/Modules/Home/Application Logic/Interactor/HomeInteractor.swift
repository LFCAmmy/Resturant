//
//  HomeInteractor.swift
//  Resturant
//
//  Created by Armaan Shrestha on 10/03/2022.
//
//

import Foundation

class HomeInteractor {
    
    // MARK: Properties
    
    weak var output: HomeInteractorOutput?
    private let service: HomeServiceType
    var taxRate = 5
    
    // MARK: Initialization
    
    init(service: HomeServiceType) {
        self.service = service
    }
    
    // MARK: Converting entities
    private func convertInput(models: [Food]) -> [FoodStructure] {
        models.map({ model -> FoodStructure in
            let strucutre = FoodStructure(id: model.id, category: model.category, name: model.name, price: model.price, image: model.image)
            return strucutre
        })
    }
}

// MARK: Home interactor input interface

extension HomeInteractor: HomeInteractorInput {
    
    func getFood() {
        let foodItem = Food.getFoodList()
        let structure = convertInput(models: foodItem)
        output?.getFoodItems(structure: structure)
    }
    
    func calculateBill(cartList: [Cart]) {
        var subTotal = 0
        for cart in cartList {
            subTotal = subTotal + (cart.qty * cart.food.price)
        }
        let taxAmount = (subTotal * taxRate) / 100
        let totalAmount = subTotal + taxAmount
        
        output?.obtained(subTotal: subTotal, taxAmount: taxAmount, totalAmount: totalAmount)
        
    }
    
    func addToCart(food: FoodViewModel, cartList: [Cart]) {
        
        var structure = cartList
        
        // to check if the food is in the cart or not
        let foodExists = structure.contains { cart in
            cart.food.id == food.id
        }
        
        /* if food exists update the quantity,
         else add food in the cart*/
        
        
        if !foodExists {
            structure.append(Cart(food: food, qty: 1))
        }else{
            // get the index of the food in cartList
            let index = structure.firstIndex(where: { cart in
                cart.food.id == food.id
            })
            structure[index!].qty = structure[index!].qty + 1
        }
        
        output?.obtained(structure: structure)
    }
    
    func removeFromCart(food: FoodViewModel, cartList: [Cart]) {
        
        var structure = cartList
        
        // to check if the food is in the cart or not
        let foodExists = structure.contains { cart in
            cart.food.id == food.id
        }
        
        //  if food exists update the quantity
        
        if foodExists {
            // get the index of the food in cartList
            let index = structure.firstIndex(where: { cart in
                cart.food.id == food.id
            })
            
            let quantity = structure[index!].qty
            
            /* if quantity is greater than 1 update the quantity,
             else remove food from the cart*/
            if quantity > 1 {
                structure[index!].qty = structure[index!].qty - 1
            } else {
                structure.remove(at: index!)
            }
        }
        
        output?.obtained(structure: structure)
    }
    
    func qtyUpdated(qty: Int, food: FoodViewModel, cartList: [Cart]) {
        var structure = cartList
        
        // to check if the food is in the cart or not
        let foodExists = structure.contains { cart in
            cart.food.id == food.id
        }
        
        /* if food exists update the quantity only if the quantity is greater than 0 else remove food from the cart,
         else add food in cart only if the quantity is greater than 0*/
        
        if !foodExists {
            if qty > 0 {
                structure.append(Cart(food: food, qty: qty))
            }
        } else {
            // get the index of the food in cartList
            let index = structure.firstIndex(where: { cart in
                cart.food.id == food.id
            })
            if qty > 0 {
                structure[index!].qty = qty
            } else {
                structure.remove(at: index!)
            }
        }
        
        output?.obtained(structure: structure)
    }
}
