//
//  Food.swift
//  Resturant
//
//  Created by Armaan Shrestha on 10/03/2022.
//

import Foundation

struct Food {
    var id: Int
    var category: String
    var name: String
    var price: Int
    var image: String
    
    static func getFoodList() -> [Food] {
        return [Food(id: 1, category: "Pizza", name: "Hawaiian Pizza", price: 650, image: "hawaiian"),
                Food(id: 2, category: "Pizza", name: "Pepperoni Pizza", price: 600, image: "pepperoni"),
                Food(id: 3, category: "Burger", name: "Cheese Burger", price: 300, image: "cheeseBurger"),
                Food(id: 4, category: "Burger", name: "Ham Burger", price: 200, image: "cheeseBurger"),
                Food(id: 5, category: "Burger", name: "Chicken Burger", price: 250, image: "cheeseBurger"),
                Food(id: 6, category: "Mo:Mo", name: "Buff Mo:Mo Fried", price: 180, image: "pepperoni"),
                Food(id: 7, category: "Pizza", name: "Veg Pizza", price: 500, image: "pepperoni"),
                Food(id: 8, category: "Pizza", name: "Hawaiian Pizza", price: 650, image: "hawaiian"),
                Food(id: 9, category: "Pizza", name: "Pepperoni Pizza", price: 600, image: "pepperoni"),
                Food(id: 10, category: "Burger", name: "Cheese Burger", price: 300, image: "cheeseBurger"),
                Food(id: 11, category: "Burger", name: "Ham Burger", price: 200, image: "cheeseBurger"),
                Food(id: 12, category: "Burger", name: "Chicken Burger", price: 250, image: "cheeseBurger"),
                Food(id: 13, category: "Mo:Mo", name: "Buff Mo:Mo Fried", price: 180, image: "pepperoni")]
    }
}
