//
//  Cart.swift
//  Resturant
//
//  Created by Armaan Shrestha on 12/03/2022.
//

import Foundation

struct Cart {
    var food : FoodViewModel
    var qty: Int    
}
