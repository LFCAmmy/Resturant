//
//  String.swift
//  Resturant
//
//  Created by Armaan Shrestha on 12/03/2022.
//

import Foundation

extension String {
    func currency() -> String {
        return "NRs." + " " + self
    }
}
